﻿using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace LabVideoGameApi.service;

public class JwtU
{
    private readonly string issue;
    private readonly string secretK;
    private readonly string roleClaim;

    public JwtU(string issuer, string secretKey, string roleClaimT)
    {
        issue = issuer;
        secretK = secretKey;
        roleClaim = roleClaimT;
    }

    public string Create(string username, string role)
    {
        var tokenHand = new JwtSecurityTokenHandler();
        var key = Encoding.ASCII.GetBytes(secretK);
        var tokenDescript = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(new[]
            {new Claim(ClaimTypes.Name, username),new Claim(roleClaim, role)}),
            Expires = DateTime.UtcNow.Add(TimeSpan.FromDays(10)),
            Issuer = issue,
            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
        };
        var token = tokenHand.CreateToken(tokenDescript);
        return tokenHand.WriteToken(token);
    }
}