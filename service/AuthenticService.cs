﻿namespace LabVideoGameApi.service;

using LabVideoGameApi.dto;
using LabVideoGameApi.model;
using LabVideoGameApi.repositorty;

public class AuthenticService : IAuthenticServ
{
    private readonly IUserRep uRepo;
    private readonly JwtU jwtU;

    public AuthenticService(IUserRep userRepository, JwtU jwtUtil)
    {
        uRepo = userRepository;
        jwtU = jwtUtil;
    }

    private string HashPsw(string password)
    {
        string hashedPassword = BCrypt.Net.BCrypt.HashPassword(password);
        return hashedPassword;
    }

    public string Login(LogDto loginDto)
    {
        var user = uRepo.GetUserByUsernameAndPassword(loginDto.Username, loginDto.Password);
        if (user == null)
        {
            return null;
        }

        return jwtU.Create(user.Username, user.Role);
    }

    public void Register(UserModel userModel)
    {
        var existingUser = uRepo.GetUserByUsername(userModel.Username);
        if (existingUser != null)
        {
            throw new InvalidOperationException("Username exist.");
        }

        string hashedPassword = HashPsw(userModel.Password);
        userModel.Password = hashedPassword;

        uRepo.AddUser(userModel);
    }
}