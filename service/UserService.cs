﻿using LabVideoGameApi.model;
using LabVideoGameApi.repositorty;

namespace LabVideoGameApi.service;

public class UserService : IUserServ
{
    private readonly IUserRep UserRep;

    public UserService(IUserRep userRepository)
    {
        UserRep = userRepository;
    }

    public IEnumerable<UserModel> GetUsers()
    {
        return UserRep.GetUsers();
    }

    public UserModel GetUserByUsername(string username)
    {
        return UserRep.GetUserByUsername(username);
    }
}