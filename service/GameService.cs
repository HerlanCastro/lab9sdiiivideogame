﻿using LabVideoGameApi.model;
using LabVideoGameApi.repositorty;

namespace LabVideoGameApi.service;

public class GameService : IGamesServ
{
    private const string GamesKey = "games";
    private readonly IGameRep gRepo;

    public GameService(IGameRep gameRepository)
    {
        gRepo = gameRepository;
    }

    public Dictionary<string, IEnumerable<GameModel>> GetAllGame()
    {
        var games = gRepo.GetAllGames();
        return new Dictionary<string, IEnumerable<GameModel>> { { GamesKey, games } };
    }

    public GameModel GetGame(Guid id)
    {
        return gRepo.GetGameById(id);
    }

    public GameModel CreateGame(GameModel game)
    {
        if (!gRepo.IsUniqueName(game.Name))
        {
            throw new InvalidOperationException("Error, similar name");
        }

        return gRepo.AddGame(game);
    }

    public GameModel UpdateGame(Guid id, GameModel game)
    {
        var existingGame = gRepo.GetGameById(id);
        if (existingGame == null)
        {
            return null;
        }

        if (!gRepo.IsUniqueName(game.Name, id))
        {
            throw new InvalidOperationException("Error, similar name");
        }

        return gRepo.UpdateGame(id, game);
    }

    public void DeleteGame(Guid id)
    {
        gRepo.DeleteGame(id);
    }
}