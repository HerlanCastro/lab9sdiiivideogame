﻿using LabVideoGameApi.dto;
using LabVideoGameApi.model;

namespace LabVideoGameApi.service;

public interface IAuthenticServ
{
    string Login(LogDto loginDto);
    void Register(UserModel userModel);
}