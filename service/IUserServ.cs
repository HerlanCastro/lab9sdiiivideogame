﻿using LabVideoGameApi.model;

namespace LabVideoGameApi.service;

public interface IUserServ
{
    IEnumerable<UserModel> GetUsers();
    UserModel GetUserByUsername(string username);
}
