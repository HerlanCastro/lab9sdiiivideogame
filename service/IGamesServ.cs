﻿using LabVideoGameApi.model;

namespace LabVideoGameApi.service;


public interface IGamesServ
{
    Dictionary<string, IEnumerable<GameModel>> GetAllGame();
    GameModel GetGame(Guid id);
    GameModel CreateGame(GameModel game);
    GameModel UpdateGame(Guid id, GameModel game);
    void DeleteGame(Guid id);
}