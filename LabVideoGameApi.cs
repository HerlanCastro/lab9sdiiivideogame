using LabVideoGameApi.DB;
using LabVideoGameApi.mapper;
using LabVideoGameApi.repositorty;
using LabVideoGameApi.service;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Filters;
using System.Text;

namespace LabVideoGameApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddSwaggerGen(options =>
            {
                options.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
                {
                    In = ParameterLocation.Header,
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey
                });

                options.OperationFilter<SecurityRequirementsOperationFilter>();
            });
            builder.Services.AddEndpointsApiExplorer();

            builder.Services.AddDbContext<GameDB>(options =>
            {
                options.UseInMemoryDatabase("GameDatabase");
            });

            builder.Services.AddAutoMapper(typeof(MapGame));
            builder.Services.AddScoped<IGameRep, GameRepository>();
            builder.Services.AddScoped<IGamesServ, GameService>();
            builder.Services.AddScoped<IUserRep, UserRepository>();
            builder.Services.AddScoped<IUserServ, UserService>();

            builder.Services.AddSingleton<JwtU>(provider =>
            {
                var configuration = provider.GetRequiredService<IConfiguration>();
                var issuer = configuration["Jwt:Issuer"];
                var secretKey = configuration["Jwt:Key"];
                var roleClaimType = "role";
                return new JwtU(issuer, secretKey, roleClaimType);
            });


            builder.Services.AddScoped<IAuthenticServ, AuthenticService>();

            builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        ValidateAudience = false,
                        ValidateIssuer = false,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(
                            builder.Configuration["Jwt:Key"]))
                    };
                });

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthorization();

            app.MapControllers();

            app.Run();
        }
    }
}
