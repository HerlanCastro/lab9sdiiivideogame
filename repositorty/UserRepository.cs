﻿using AutoMapper;
using LabVideoGameApi.DB;
using LabVideoGameApi.model;

namespace LabVideoGameApi.repositorty;
public class UserRepository : IUserRep
{
    private readonly GameDB DbContx;
    private readonly IMapper Mapp;

    public UserRepository(GameDB dbContext, IMapper mapper)
    {
        DbContx = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        Mapp = mapper ?? throw new ArgumentNullException(nameof(mapper));
    }

    public IEnumerable<UserModel> GetUsers()
    {
        var users = DbContx.Users.ToList();
        return Mapp.Map<List<UserModel>>(users);
    }

    public UserModel AddUser(UserModel userModel)
    {
        var user = Mapp.Map<UserModel>(userModel);
        DbContx.Users.Add(user);
        DbContx.SaveChanges();
        return Mapp.Map<UserModel>(user);
    }

    public UserModel GetUserByUsername(string username)
    {
        var user = DbContx.Users.FirstOrDefault(u => u.Username == username);
        return Mapp.Map<UserModel>(user);
    }

    public UserModel GetUserByUsernameAndPassword(string username, string password)
    {
        var user = DbContx.Users.FirstOrDefault(u => u.Username == username);
        if (user == null)
        {
            return null;
        }
        bool passwordMatches = BCrypt.Net.BCrypt.Verify(password, user.Password);
        return passwordMatches ? Mapp.Map<UserModel>(user) : null;
    }


}