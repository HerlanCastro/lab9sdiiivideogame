﻿using LabVideoGameApi.model;

namespace LabVideoGameApi.repositorty;

public interface IUserRep
{
    IEnumerable<UserModel> GetUsers();
    UserModel AddUser(UserModel user);
    UserModel GetUserByUsernameAndPassword(string username, string password);
    UserModel GetUserByUsername(string username);
}