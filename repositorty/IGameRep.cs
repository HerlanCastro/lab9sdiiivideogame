﻿using LabVideoGameApi.model;

namespace LabVideoGameApi.repositorty;

public interface IGameRep
{
    IEnumerable<GameModel> GetAllGames();
    GameModel GetGameById(Guid id);
    GameModel AddGame(GameModel game);
    GameModel UpdateGame(Guid id, GameModel game);
    void DeleteGame(Guid id);
    bool IsUniqueName(string name, Guid? excludeId = null);
}