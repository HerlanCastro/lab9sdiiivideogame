﻿namespace LabVideoGameApi.repositorty;

using AutoMapper;
using LabVideoGameApi.DB;
using LabVideoGameApi.model;
using System;
using System.Collections.Generic;
using System.Linq;

public class GameRepository : IGameRep
{
    private readonly GameDB DbContx;
    private readonly IMapper Mapp;

    public GameRepository(GameDB dbContext, IMapper mapper)
    {
        DbContx = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        Mapp = mapper ?? throw new ArgumentNullException(nameof(mapper));
    }

    public GameModel GetGameById(Guid id)
    {
        var game = DbContx.Games.FirstOrDefault(g => g.Id == id);
        return Mapp.Map<GameModel>(game);
    }

    public IEnumerable<GameModel> GetAllGames()
    {
        var games = DbContx.Games.ToList();
        return Mapp.Map<List<GameModel>>(games);
    }

    public GameModel AddGame(GameModel gameModel)
    {
        var game = Mapp.Map<GameModel>(gameModel);
        DbContx.Games.Add(game);
        DbContx.SaveChanges();
        return Mapp.Map<GameModel>(game);
    }

    public GameModel UpdateGame(Guid id, GameModel gameModel)
    {
        var existingGame = DbContx.Games.FirstOrDefault(g => g.Id == id);
        existingGame.Name = gameModel.Name;
        existingGame.Platform = gameModel.Platform;
        existingGame.Price = gameModel.Price;

        DbContx.SaveChanges();

        return Mapp.Map<GameModel>(existingGame);
    }


    public bool IsUniqueName(string name, Guid? excludeId = null)
    {
        return !DbContx.Games.Any(g => g.Name == name && g.Id != excludeId);
    }

    public void DeleteGame(Guid id)
    {
        var game = DbContx.Games.FirstOrDefault(g => g.Id == id);
        if (game != null)
        {
            DbContx.Games.Remove(game);
            DbContx.SaveChanges();
        }
    }

}