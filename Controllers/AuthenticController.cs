﻿using LabVideoGameApi.dto;
using LabVideoGameApi.model;
using LabVideoGameApi.service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;

namespace LabVideoGameApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticController : ControllerBase
    {
        private readonly IAuthenticServ authService;

        public AuthenticController(IAuthenticServ authService)
        {
            this.authService = authService;
        }

        [HttpPost("register")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult Register(UserModel userModel)
        {
            try
            {
                authService.Register(userModel);
                return StatusCode(StatusCodes.Status201Created);
            }
            catch (InvalidOperationException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost("login")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult Login(LogDto loginDto)
        {
            try
            {
                var token = authService.Login(loginDto);
                if (token == null)
                    return Unauthorized();

                return Ok(new { token });
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
