﻿using LabVideoGameApi.service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace LabVideoGameApi.Controllers;

[Route("api/[controller]")]
[ApiController]
[Authorize(Roles = "Admin,User")]
public class UserController : ControllerBase
{
    private readonly IUserServ uService;

    public UserController(IUserServ userService)
    {
        uService = userService;
    }

    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public IActionResult GetAllUsers()
    {
        try
        {
            var users = uService.GetUsers();
            if (!users.Any())
            {
                return NotFound("No users found.");
            }

            var result = new { users };

            return Ok(result);
        }
        catch (Exception)
        {
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }

}