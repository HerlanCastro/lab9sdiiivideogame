﻿using AutoMapper;
using LabVideoGameApi.model;

namespace LabVideoGameApi.mapper;


public class MapGame : Profile
{
    public MapGame()
    {
        CreateMap<GameModel, GameModel>().ReverseMap();
    }
}