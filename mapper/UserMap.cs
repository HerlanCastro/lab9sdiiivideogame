﻿using AutoMapper;
using LabVideoGameApi.dto;
using LabVideoGameApi.model;

namespace LabVideoGameApi.mapper;
public class UserMap : Profile
{
    public UserMap()
    {
        CreateMap<UserModel, UserModel>().ReverseMap();
        CreateMap<LogDto, UserModel>().ReverseMap();
    }
}