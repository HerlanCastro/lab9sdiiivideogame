﻿namespace LabVideoGameApi.DB;

using LabVideoGameApi.model;
using Microsoft.EntityFrameworkCore;


public class GameDB : DbContext
{
    public DbSet<GameModel> Games { get; set; }
    public DbSet<UserModel> Users { get; set; }

    public GameDB(DbContextOptions<GameDB> options) : base(options)
    {
        if (!Games.Any())
        {

            Games.AddRange(
                new List<GameModel>
                {
                     new GameModel { Name = "Fifita", Platform = "Xbox", Price = 20.99m },
                }
            );
            SaveChanges();
        }
    }
}
