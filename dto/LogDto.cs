﻿namespace LabVideoGameApi.dto;

public class LogDto
{
    public string Username { get; set; }
    public string Password { get; set; }
}