﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LabVideoGameApi.model;

[Table("Games")]
[Index(nameof(Name), IsUnique = true)]
public class GameModel
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public Guid Id { get; set; }

    [Required(ErrorMessage = "Name is required.")]
    [StringLength(30, ErrorMessage = "The game name cannot be more than 30 characters.")]
    public string Name { get; set; }

    [Required(ErrorMessage = "Game platform is required.")]
    [RegularExpression("^(Xbox|Play Station)$", ErrorMessage = "The platform can only be Xbox or Playstation.")]
    public string Platform { get; set; }

    [Required(ErrorMessage = "Price is required")]
    [Range(0.01, double.MaxValue, ErrorMessage = "The game price must be greater than zero.")]
    public decimal Price { get; set; }
}
